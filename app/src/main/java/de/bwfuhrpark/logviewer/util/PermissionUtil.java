package de.bwfuhrpark.logviewer.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

/**
 * Util-Klasse zur Anfrage von Rechten.
 * <p>
 * Die aufrufende Activity/Fragment muss in der Methode {@link Activity#onRequestPermissionsResult(int, String[], int[])} implementieren,
 * was passieren soll, falls das Recht gewährt oder auch nicht gewährt wurde.
 */
public final class PermissionUtil {

    /**
     * Privater Konstruktor.
     * <p>
     * Soll nicht instanziierbar sein.
     */
    private PermissionUtil() {
        // Soll nicht instanziierbar sein.
    }

    /**
     * Fragt das Recht an um auf die SD-Karte lesend zuzugreifen.
     * <p>
     * Weitere Info siehe {@link PermissionUtil#requestPermission(Activity, String, int)}.
     *
     * @param activity    Die  Activity, in welcher das Recht benötigt wird.
     * @param requestCode Der Request-Code für diese Anfrage. Dieser wird in der Methode
     *                    {@code Activity#onRequestPermissionsResult(int, String[], int[])} mit zurückgegeben.
     * @return {@code true}, falls das Recht bereits gewährt wurde. Ansonsten {@code false}.
     */
    public static boolean requestPermissionReadExternalStorage(final Activity activity, final int requestCode) {
        return requestPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE, requestCode);
    }

    /**
     * Fragt das Recht an um auf die SD-Karte lesend zuzugreifen.
     * <p>
     * Weitere Info siehe {@link PermissionUtil#requestPermission(Activity, String, int)}.
     *
     * @param fragment    Das Fragment, in welcher das Recht benötigt wird.
     * @param requestCode Der Request-Code für diese Anfrage. Dieser wird in der Methode
     *                    {@code Activity#onRequestPermissionsResult(int, String[], int[])} mit zurückgegeben.
     * @return {@code true}, falls das Recht bereits gewährt wurde. Ansonsten {@code false}.
     */
    public static boolean requestPermissionReadExternalStorage(final Fragment fragment, final int requestCode) {
        return requestPermission(fragment, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
    }

    /**
     * Fragt das Recht an um auf die SD-Karte schreibend zuzugreifen.
     * <p>
     * Weitere Info siehe {@link PermissionUtil#requestPermission(Activity, String, int)}.
     *
     * @param activity    Die  Activity, in welcher das Recht benötigt wird.
     * @param requestCode Der Request-Code für diese Anfrage. Dieser wird in der Methode
     *                    {@code Activity#onRequestPermissionsResult(int, String[], int[])} mit zurückgegeben.
     * @return {@code true}, falls das Recht bereits gewährt wurde. Ansonsten {@code false}.
     */
    public static boolean requestPermissionWriteExternalStorage(final Activity activity, final int requestCode) {
        return requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, requestCode);
    }

    /**
     * Fragt das Recht an um auf die SD-Karte schreibend zuzugreifen.
     * <p>
     * Weitere Info siehe {@link PermissionUtil#requestPermission(Activity, String, int)}.
     *
     * @param fragment    Die  Activity, in welcher das Recht benötigt wird.
     * @param requestCode Der Request-Code für diese Anfrage. Dieser wird in der Methode
     *                    {@code Activity#onRequestPermissionsResult(int, String[], int[])} mit zurückgegeben.
     * @return {@code true}, falls das Recht bereits gewährt wurde. Ansonsten {@code false}.
     */
    public static boolean requestPermissionWriteExternalStorage(final Fragment fragment, final int requestCode) {
        return requestPermission(fragment, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
    }

    /**
     * Fragt das Recht an um die Kamera zu verwenden.
     * <p>
     * Weitere Info siehe {@link PermissionUtil#requestPermission(Activity, String, int)}.
     *
     * @param activity    Die  Activity, in welcher das Recht benötigt wird.
     * @param requestCode Der Request-Code für diese Anfrage. Dieser wird in der Methode
     *                    {@code Activity#onRequestPermissionsResult(int, String[], int[])} mit zurückgegeben.
     * @return {@code true}, falls das Recht bereits gewährt wurde. Ansonsten {@code false}.
     */
    public static boolean requestPermissionCamera(final Activity activity, final int requestCode) {
        return requestPermission(activity, Manifest.permission.CAMERA, requestCode);
    }

    /**
     * Fragt das übergebene Recht an. Wurde das Recht in der Vergangenheit noch nicht gewährt, wird {@code false} zurückgegegeben und die
     * Methode {@link Activity#onRequestPermissionsResult(int, String[], int[])} der übergebenen Activity ausgeführt.
     * <p>
     * Wurde das Recht bereits in der Vergangenheit gewährt, wird {@code true} zurückgegeben. Die Methode
     * {@code Activity#onRequestPermissionsResult(int, String[], int[])} wird dann nicht ausgeführt.
     *
     * @param activity    Die  Activity, in welcher das Recht benötigt wird.
     * @param permission  Das gewünschte Recht (siehe {@link Manifest.permission}).
     * @param requestCode Der Request-Code für diese Anfrage. Dieser wird in der Methode
     *                    {@code Activity#onRequestPermissionsResult(int, String[], int[])} mit zurückgegeben.
     * @return {@code true}, falls das Recht bereits gewährt wurde. Ansonsten {@code false}.
     */
    private static boolean requestPermission(final Activity activity, final String permission, final int requestCode) {
        boolean hasNoPermission;
        if (hasNoPermission = ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
        }
        return !hasNoPermission;
    }

    private static boolean requestPermission(final Activity activity, final String[] permission, final int requestCode) {
        boolean hasPermission = true;
        for (String perm : permission) {
            hasPermission = hasPermission && ContextCompat.checkSelfPermission(activity, perm) == PackageManager.PERMISSION_GRANTED;
        }
        if (!hasPermission) {
            ActivityCompat.requestPermissions(activity, permission, requestCode);
        }
        return hasPermission;
    }

    private static boolean requestPermission(final Fragment fragment, final String[] permission, final int requestCode) {
        boolean hasPermission = true;
        for (String perm : permission) {
            hasPermission = hasPermission && ContextCompat.checkSelfPermission(fragment.getContext(), perm) == PackageManager.PERMISSION_GRANTED;
        }
        if (!hasPermission) {
            fragment.requestPermissions(permission, requestCode);
        }
        return hasPermission;
    }

}
