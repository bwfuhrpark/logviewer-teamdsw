package de.bwfuhrpark.logviewer.exception;

import java.util.Locale;

import static java.lang.String.format;

/**
 * Projekt-spezifische Exception.
 */
public class ProjectException extends RuntimeException {

    public ProjectException(final String message, final Object... params) {
        super(message == null ? null : format(Locale.GERMAN, message, params));
    }

    public ProjectException(final Throwable cause, final String message, final Object... params) {
        super(message == null ? null : format(Locale.GERMAN, message, params), cause);
    }

}
