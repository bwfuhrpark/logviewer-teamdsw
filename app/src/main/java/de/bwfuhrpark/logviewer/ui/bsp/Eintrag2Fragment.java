package de.bwfuhrpark.logviewer.ui.bsp;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import de.bwfuhrpark.logviewer.R;
import de.bwfuhrpark.logviewer.util.PermissionUtil;

public class Eintrag2Fragment extends Fragment {

    private final static int REQUEST_PERMISSION = 1;

    private EditText textPfad;
    private TextView textViewAnzeige;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.frg_bsp_eintrag2_file, container, false);
        textPfad = view.findViewById(R.id.textDateiPfad);
        Button buttonLaden = view.findViewById(R.id.buttonLaden);
        textViewAnzeige = view.findViewById(R.id.textViewAnzeige);

        buttonLaden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PermissionUtil.requestPermissionReadExternalStorage(Eintrag2Fragment.this, REQUEST_PERMISSION)) {
                    ladeTextDatei(textPfad, textViewAnzeige);
                }
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ladeTextDatei(textPfad, textViewAnzeige);
                }
        }
    }

    private void ladeTextDatei(EditText textPfad, TextView textViewAnzeige) {
        File externalStorage = Environment.getExternalStorageDirectory();
        File file = new File(externalStorage, textPfad.getText().toString());

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            StringBuilder inhalt = new StringBuilder();
            while ((line = br.readLine()) != null) {
                inhalt.append(line).append("\n");
            }
            textViewAnzeige.setText(inhalt.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
            }
        }
    }
}
