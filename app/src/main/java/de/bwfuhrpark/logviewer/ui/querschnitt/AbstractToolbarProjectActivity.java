package de.bwfuhrpark.logviewer.ui.querschnitt;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import de.bwfuhrpark.logviewer.R;

/**
 * Activity mit einer Toolbar. Im Layout der implementierenden Activity muss
 * "<include layout="@layout/toolbar"/>" enthalten sein.
 *
 * Die Toolbar wird automatisch für diese Activity gesetzt.
 */
public abstract class AbstractToolbarProjectActivity extends AbstractProjectActivity {

    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        toolbar = findViewById(R.id.toolbar);
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void setSupportActionBar() {
        setSupportActionBar(toolbar);
    }

    /**
     * Setzt einen Subtitle in der Toolbar
     */
    protected void setSubtitle(final String subtitle) {
        toolbar.setSubtitle(subtitle);
    }
}
