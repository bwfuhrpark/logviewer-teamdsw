package de.bwfuhrpark.logviewer.ui.querschnitt;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;

import de.bwfuhrpark.logviewer.R;
import de.bwfuhrpark.logviewer.exception.ProjectException;
import de.bwfuhrpark.logviewer.ui.bsp.Eintrag1Fragment;
import de.bwfuhrpark.logviewer.ui.bsp.Eintrag2Fragment;

/**
 * Start Activity zur Anzeige des Hauptbildschir inkl. eines Seitenmemüs
 */
public class MainActivity extends AbstractToolbarProjectActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.act_querschnitt_main);
        super.onCreate(savedInstanceState);

        //Seitenmenü Layout und NavigationView
        drawerLayout = findViewById(R.id.act_querschnitt_main_drawer_layout);
        navigationView = findViewById(R.id.act_querschnitt_main_navigation_view);


        // Verknüpfe ActionBar-Icon mit DrawerLayout.
        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.txt_drawer_open,
                R.string.txt_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        // Registriere OnNavigationItemSelectedListener.
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {
                selectDrawerItem(menuItem.getItemId());
                return true;
            }
        });

        //ersten Menüeintrag automatisch auswählen
        selectDrawerItem(R.id.drawer_eintrag_1);
    }

    /**
     * Selektiert das gewünschte Element im Drawer.
     *
     * @param id Die ID des zu selektierenden Elements.
     */
    private void selectDrawerItem(final int id) {
        Fragment fragment = null;
        switch (id) {
            case R.id.drawer_eintrag_1:
                fragment = new Eintrag1Fragment();
                setTitle(getString(R.string.ttl_frg_eintrag_1));
                break;
            case R.id.drawer_eintrag_2:
                fragment = new Eintrag2Fragment();
                setTitle(getString(R.string.ttl_frg_eintrag_2));
                break;
            default:
                throw new ProjectException(getString(R.string.exc_act_querschnitt_main_menupunkt_existiert_nicht));
        }

        // Tausche Fragment der Main-Activity aus.
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.act_querschnitt_main_frame_layout, fragment).commit();
        }

        // Aktuellen Eintrag markieren.
        navigationView.setCheckedItem(id);

        // Schließe Drawer.
        drawerLayout.closeDrawer(GravityCompat.START);
    }

}
