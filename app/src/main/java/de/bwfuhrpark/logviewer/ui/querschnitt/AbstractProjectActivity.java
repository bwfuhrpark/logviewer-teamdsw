package de.bwfuhrpark.logviewer.ui.querschnitt;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import de.bwfuhrpark.logviewer.R;

/**
 * Vorlage für alle Activities.
 * Setzt die ActionBar und gibt die Möglichkeit die Statusbar Farbe zu setzen.
 */
public abstract class AbstractProjectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle();
        setSupportActionBar();
        enableHomeButton();

    }

    /**
     * Setzt die Titelzeile der Activity.
     */
    protected void setTitle() {
        // Leere Implementierung.
    }

    /**
     * Setzt die ActionBar der Activity.
     */
    protected void setSupportActionBar() {
        // Leere Implementierung.
    }

    /**
     * Setzt die Farbe der StatusBar auf R.color.primary_dark.
     * <p>
     * Workaround für eine funktionierende Färbung der StatusBar.
     */
    protected void enableStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    /**
     * Aktiviert den Home-Button, welcher zur vorherigen Activity springen soll.
     */
    protected void enableHomeButton() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
